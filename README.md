**NOTE:**

The now Blackcoin Legacy client is the original client created by rat4. It is based on the BTC 0.9+ code, and will not be updated. 

You can compile the binaries yourself from the current master branch, if needed, and some connectivity issues have been solved on that branch.
If for some reason you need the older binaries you can download them from the page below, be aware of sync and connectivity issues!
https://github.com/CoinBlack/blackcoin/releases

If you need to sync from scratch it can take a long time, and we advice you to use a bootstrap file.

After the dust/spam attack, late September 2020, we noticed that the Blackcoin Original software was causing too many issues,
and now we ask people to migrate to the new Blackcoin More client.

Blackcoin More is our stable client and is based on the newer BTC 0.13+ code. The code will be updated frequently. 
**Please visit https://blackcoinmore.org/ for more info**

The binaries can also be downloaded from here https://gitlab.com/blackcoin/blackcoin-more/-/releases#v2.13.2.6

Visit one of our community support channels if you need help. 

https://blackcoin.org/ for info on the Blackcoin Protocol, Projects, and Support Channels

https://blackcoinmore.org/ for info on the Blackcoin More project.

https://blackcoin.nl/ the most helpful community of blackcoin enthusiasts.

*The Blackcoin Team*